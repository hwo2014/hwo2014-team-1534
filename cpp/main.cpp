#include <iostream>
#include <string>
#include <jsoncons/json.hpp>
#include "Protocol.h"
#include "Connection.h"
#include "GameLogic.h"

void Run(HWO::CConnection& rcConnection, const std::string& strName, const std::string& strKey)
{
	HWO::CGameLogic cGame;
	rcConnection.SendRequests({ HWO::Protocol::MakeJoin(strName, strKey) });

	while (true)
	{
		boost::system::error_code cError;
		jsoncons::json cResponse = rcConnection.ReceiveResponse(cError);

		if (cError == boost::asio::error::eof)
		{
			std::cout << "Connection closed" << std::endl;
			break;
		}
		else if (cError)
		{
			throw boost::system::system_error(cError);
		}

		rcConnection.SendRequests(cGame.React(cResponse));
	}
}

int main(int argc, const char* argv[])
{
	try
	{
		if (argc != 5)
		{
			std::cerr << "Usage: ./run host port botname botkey" << std::endl;
			return 1;
		}

		const std::string strHost(argv[1]);
		const std::string strPort(argv[2]);
		const std::string strName(argv[3]);
		const std::string strKey(argv[4]);
		std::cout << "Host: " << strHost << ", port: " << strPort << ", name: " << strName << ", key:" << strKey << std::endl;

		HWO::CConnection cConnection(strHost, strPort);
		Run(cConnection, strName, strKey);
	}
	catch (const std::exception& cExcepion)
	{
		std::cerr << cExcepion.what() << std::endl;
		return 2;
	}

	return 0;
}
