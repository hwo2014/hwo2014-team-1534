#include "GameLogic.h"
#include "Protocol.h"

HWO::CGameLogic::CGameLogic():
m_cActionMap
{
	{ "join", &CGameLogic::OnJoin },
	{ "yourCar", &CGameLogic::OnYourCar },
	{ "gameInit", &CGameLogic::OnGameInit },
	{ "gameStart", &CGameLogic::OnGameStart },
	{ "carPositions", &CGameLogic::OnCarPositions },
	{ "crash", &CGameLogic::OnCrash },
	{ "gameEnd", &CGameLogic::OnGameEnd },
	{ "error", &CGameLogic::OnError }
},
m_nLastTrackPieceIndex(0),
m_dLastInPieceDistance(0),
m_dSpeed(0.0)
{
	memset(&m_sCarInfo, 0, sizeof(SCarInfo));
}

HWO::CGameLogic::~CGameLogic()
{
}

HWO::CGameLogic::MessageVector HWO::CGameLogic::React(const jsoncons::json& cMessage)
{
	const std::string& strMessageType = cMessage["msgType"].as<std::string>();
	auto cActionItem = m_cActionMap.find(strMessageType);
	if (cActionItem != m_cActionMap.end())
	{
		return (cActionItem->second)(this, cMessage);
	}
	else
	{
		std::cout << "Unknown message type: " << strMessageType << std::endl;
		return { Protocol::MakePing() };
	}
}

const HWO::CGameLogic::SLane& HWO::CGameLogic::GetLaneByIndex(int nIndex) const
{
	for (auto it = m_cLanes.begin(); it != m_cLanes.end(); ++it)
	{
		if (it->m_nIndex == nIndex)
		{
			return *it;
		}
	}
	return m_cLanes.at(0);
}

double HWO::CGameLogic::CalculateTrackPieceLength(int nPieceIndex, int nLaneIndex) const
{
	const STrackPiece& sPiece = m_cTrackPieces.at(nPieceIndex);

	if (sPiece.m_eType == Straight)
	{
		return sPiece.m_dLength;
	}
	else
	{
		// convert angle to radians
		const double kdPi = 3.141592653589793238463;
		const double kdDegreesToRadians = kdPi / 180.0;
		const double kdSign = sPiece.m_dAngle > 0.0 ? 1.0 : -1.0;
		const double kdAngle = fabs(sPiece.m_dAngle) * kdDegreesToRadians;

		// find current lane's distance from track's center line
		double dDistanceFromCenter = GetLaneByIndex(nLaneIndex).m_dDistanceFromCenter;

		// calculate true radius
		const double kdRadius = sPiece.m_dRadius - dDistanceFromCenter * kdSign;

		// 2 * pi * r
		return kdAngle * kdRadius;
	}
}

void HWO::CGameLogic::ParseTrack(const jsoncons::json& cTrack)
{
	ParseTrackPieces(cTrack["pieces"]);
	ParseLanes(cTrack["lanes"]);
}

void HWO::CGameLogic::ParseTrackPieces(const jsoncons::json& cPieces)
{
	for (auto it = cPieces.begin_elements(); it != cPieces.end_elements(); ++it)
	{
		STrackPiece sPiece;
		memset(&sPiece, 0, sizeof(STrackPiece));
		sPiece.m_eType = it->has_member("length") ? Straight : Bend;
		for (auto m = it->begin_members(); m != it->end_members(); ++m)
		{
			const std::string& strName = m->name();
			if (strName == "length")
			{
				sPiece.m_dLength = m->value().as<double>();
			}
			else if (strName == "radius")
			{
				sPiece.m_dRadius = m->value().as<double>();
			}
			else if (strName == "angle")
			{
				sPiece.m_dAngle = m->value().as<double>();
			}
			else if (strName == "switch")
			{
				sPiece.m_bCanSwitchLane = m->value().as<bool>();
			}
			else
			{
				std::cout << "Unrecognized track piece attribute: " << strName << std::endl;
			}
		}
		m_cTrackPieces.push_back(sPiece);
	}
}

void HWO::CGameLogic::ParseLanes(const jsoncons::json& cLanes)
{
	for (auto it = cLanes.begin_elements(); it != cLanes.end_elements(); ++it)
	{
		SLane sLane;
		memset(&sLane, 0, sizeof(SLane));
		for (auto m = it->begin_members(); m != it->end_members(); ++m)
		{
			const std::string& strName = m->name();
			if (strName == "distanceFromCenter")
			{
				sLane.m_dDistanceFromCenter = m->value().as<double>();
			}
			else if (strName == "index")
			{
				sLane.m_nIndex = m->value().as<int>();
			}
			else
			{
				std::cout << "Unrecognized lane attribute: " << strName << std::endl;
			}
		}
		m_cLanes.push_back(sLane);
	}
	// TODO: sort lanes by index?
}

void HWO::CGameLogic::ParseCars(const jsoncons::json& cCars)
{
	for (auto it = cCars.begin_elements(); it != cCars.end_elements(); ++it)
	{
		const jsoncons::json& cCar = *it;
		const jsoncons::json& cId = cCar["id"];
		const std::string& strColor = cId["color"].as<std::string>();
		if (strColor == m_strColor)
		{
			const jsoncons::json& cDimensions = cCar["dimensions"];
			m_sCarInfo.m_dLength = cDimensions["length"].as<double>();
			m_sCarInfo.m_dWidth = cDimensions["width"].as<double>();
			m_sCarInfo.m_dGuideFlagPosition = cDimensions["guideFlagPosition"].as<double>();
			std::cout << "Your car is " << strColor << std::endl;
			break;
		}
		else
		{
			continue;
		}
	}
}

double HWO::CGameLogic::CalculateCarSpeed(const jsoncons::json& cCar)
{
	const jsoncons::json& cPiecePosition = cCar["piecePosition"];
	const jsoncons::json& cLane = cPiecePosition["lane"];
	const int knTrackPieceIndex = cPiecePosition["pieceIndex"].as<int>();
	const int knLaneIndex = cLane["startLaneIndex"].as<int>();

	double dSpeed = 0.0;
	const double kdInPieceDistance = cPiecePosition["inPieceDistance"].as<double>();
	const STrackPiece& cPiece = m_cTrackPieces.at(knTrackPieceIndex);
	if (knTrackPieceIndex == m_nLastTrackPieceIndex)
	{
		dSpeed = kdInPieceDistance - m_dLastInPieceDistance;
	}
	else
	{
		const double kdLastPieceLength = CalculateTrackPieceLength(m_nLastTrackPieceIndex, knLaneIndex);
		const double kdLastPieceDistanceDeficit = kdLastPieceLength - m_dLastInPieceDistance;
		dSpeed = kdLastPieceDistanceDeficit + kdInPieceDistance;
	}

	const double kdPieceLength = CalculateTrackPieceLength(knTrackPieceIndex, knLaneIndex);
	//printf("Speed: %.5f | ", dSpeed);
	//printf("Indices: %3d->%3d | ", m_nLastTrackPieceIndex, knTrackPieceIndex);
	//printf("Progress: %.2f/%.2f\n", kdInPieceDistance, kdPieceLength);

	m_dLastInPieceDistance = kdInPieceDistance;
	m_nLastTrackPieceIndex = knTrackPieceIndex;

	return dSpeed;
}

void HWO::CGameLogic::GetNextBendInfo(int nTrackPieceIndex, int nLaneIndex, int* pnBendIndex, double* pdDistance) const
{
	*pdDistance = 0.0;
	const int knPieceCount = m_cTrackPieces.size();
	for (int i = 0; i < knPieceCount; ++i)
	{
		++nTrackPieceIndex;
		if (nTrackPieceIndex >= knPieceCount)
		{
			nTrackPieceIndex = 0;
		}
		const STrackPiece& sPiece = m_cTrackPieces.at(nTrackPieceIndex);
		if (sPiece.m_eType == Bend)
		{
			*pnBendIndex = nTrackPieceIndex;
			return;
		}
		*pdDistance += CalculateTrackPieceLength(nTrackPieceIndex, nLaneIndex);
	}
	*pnBendIndex = -1;
	*pdDistance = -1.0;
}

HWO::CGameLogic::MessageVector HWO::CGameLogic::OnJoin(const jsoncons::json& cMessage)
{
	std::cout << "Joined" << std::endl;
	return { Protocol::MakePing() };
}

HWO::CGameLogic::MessageVector HWO::CGameLogic::OnYourCar(const jsoncons::json& cMessage)
{
	const jsoncons::json& cData = cMessage["data"];
	m_strName = cData["name"].as<std::string>();
	m_strColor = cData["color"].as<std::string>();
	std::cout << "Your car is " << m_strColor << std::endl;
	return { Protocol::MakePing() };
}

HWO::CGameLogic::MessageVector HWO::CGameLogic::OnGameInit(const jsoncons::json& cMessage)
{
	std::cout << "Race initialized" << std::endl;

	const jsoncons::json& cData = cMessage["data"];
	const jsoncons::json& cRace = cData["race"];

	ParseTrack(cRace["track"]);
	ParseCars(cRace["cars"]);

	return { Protocol::MakePing() };
}

HWO::CGameLogic::MessageVector HWO::CGameLogic::OnGameStart(const jsoncons::json& cMessage)
{
	std::cout << "Race started" << std::endl;
	return { Protocol::MakePing() };
}

HWO::CGameLogic::MessageVector HWO::CGameLogic::OnCarPositions(const jsoncons::json& cMessage)
{
	if (cMessage.has_member("gameTick"))
	{
		const int knGameTick = cMessage["gameTick"].as<int>();

		// HACK: replace with proper code which determines lane closest to center
		if (knGameTick == 1)
		{
			std::cout << "Switch lane to right" << std::endl;
			return { Protocol::MakeSwitchLane("Right") };
		}
	}

	const jsoncons::json& cData = cMessage["data"];

	// find our car
	for (auto it = cData.begin_elements(); it != cData.end_elements(); ++it)
	{
		const jsoncons::json& cCar = *it;
		const jsoncons::json& cId = cCar["id"];
		const std::string& strColor = cId["color"].as<std::string>();
		if (strColor == m_strColor)
		{
			m_dSpeed = CalculateCarSpeed(cCar);

			const jsoncons::json& cPiecePosition = cCar["piecePosition"];
			const jsoncons::json& cLane = cPiecePosition["lane"];
			const int knTrackPieceIndex = cPiecePosition["pieceIndex"].as<int>();
			const int knLaneIndex = cLane["startLaneIndex"].as<int>();
			
			int nNextBendIndex = 0;
			double dDistanceToNextBend = 0.0;
			GetNextBendInfo(knTrackPieceIndex, knLaneIndex, &nNextBendIndex, &dDistanceToNextBend);
			const STrackPiece& cPiece = m_cTrackPieces.at(knTrackPieceIndex);
			const double kdCurrentPieceDeficit = CalculateTrackPieceLength(knTrackPieceIndex, knLaneIndex) - m_dLastInPieceDistance;
			dDistanceToNextBend += kdCurrentPieceDeficit;
			
			const double knSafeBrakeCoef = 20.0;
			if ((nNextBendIndex == -1) || (dDistanceToNextBend > m_dSpeed * knSafeBrakeCoef))
			{
				return { Protocol::MakeThrottle(1.0) };
			}
			else
			{
				const STrackPiece& cNextBendPiece = m_cTrackPieces.at(nNextBendIndex);
				const double kdBendDifficultyCoef = sqrt(cNextBendPiece.m_dRadius);
				const double kdSafetyCoef = 0.645;
				const double kdSafeSpeed = kdBendDifficultyCoef * kdSafetyCoef;
				//std::cout << cNextBendPiece.m_dRadius << ", Coef: " << kdBendDifficultyCoef << " Safe speed: " << kdSafeSpeed;
				if (m_dSpeed > kdSafeSpeed)
				{
					//std::cout << " - Stopping..." << std::endl;
					return { Protocol::MakeThrottle(0.0) };
				}
				else
				{
					//std::cout << " - Still going!" << std::endl;
					return { Protocol::MakeThrottle(1.0) };
				}
			}

			break;
		}
		else
		{
			continue;
		}
	}

	return { Protocol::MakeThrottle(1.0) };
}

HWO::CGameLogic::MessageVector HWO::CGameLogic::OnCrash(const jsoncons::json& cMessage)
{
	std::cout << "Someone crashed" << std::endl;
	return { Protocol::MakePing() };
}

HWO::CGameLogic::MessageVector HWO::CGameLogic::OnGameEnd(const jsoncons::json& cMessage)
{
	std::cout << "Race ended" << std::endl;
	return { Protocol::MakePing() };
}

HWO::CGameLogic::MessageVector HWO::CGameLogic::OnError(const jsoncons::json& cMessage)
{
	const std::string& strError = cMessage["data"].to_string();
	std::cout << "Error: " << strError << std::endl;
	return { Protocol::MakePing() };
}

