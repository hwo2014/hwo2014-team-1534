#include "Connection.h"

HWO::CConnection::CConnection(const std::string& strHost, const std::string& strPort):
m_cSocket(m_cIoService)
{
	boost::asio::ip::tcp::resolver cResolver(m_cIoService);
	boost::asio::ip::tcp::resolver::query cQuery(strHost, strPort);
	boost::asio::connect(m_cSocket, cResolver.resolve(cQuery));
	m_cResponseBuffer.prepare(8192);
}

HWO::CConnection::~CConnection()
{
	m_cSocket.close();
}

jsoncons::json HWO::CConnection::ReceiveResponse(boost::system::error_code& rcError)
{
	boost::asio::read_until(m_cSocket, m_cResponseBuffer, "\n", rcError);
	if (rcError)
	{
		return jsoncons::json();
	}
	std::istream cStream(&m_cResponseBuffer);
	return jsoncons::json::parse(cStream);
}

void HWO::CConnection::SendRequests(const std::vector<jsoncons::json>& cMessages)
{
	jsoncons::output_format cFormat;
	cFormat.escape_all_non_ascii(true);
	boost::asio::streambuf cRequestBuffer;
	std::ostream cStream(&cRequestBuffer);
	for (const jsoncons::json& cMessage: cMessages)
	{
		cMessage.to_stream(cStream, cFormat);
		cStream << std::endl;
	}
	m_cSocket.send(cRequestBuffer.data());
}

