#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

namespace HWO
{
	class CGameLogic
	{
	private:
		enum ETrackPieceType
		{
			Straight = 0,
			Bend
		};

		struct STrackPiece
		{
			ETrackPieceType m_eType;
			double m_dLength;
			double m_dRadius;
			double m_dAngle;
			bool m_bCanSwitchLane;
		};

		struct SLane
		{
			double m_dDistanceFromCenter;
			int m_nIndex;
		};

		struct SCarInfo
		{
			double m_dWidth;
			double m_dLength;
			double m_dGuideFlagPosition;
		};

		typedef std::vector<jsoncons::json> MessageVector;
		typedef std::function<MessageVector(CGameLogic*, const jsoncons::json&)> ActionFunction;

	public:
		CGameLogic();
		~CGameLogic();

		MessageVector React(const jsoncons::json& cMessage);

	private:
		const SLane& GetLaneByIndex(int nIndex) const;
		double CalculateTrackPieceLength(int nPieceIndex, int nLaneIndex) const;
		void ParseTrack(const jsoncons::json& cTrack);
		void ParseTrackPieces(const jsoncons::json& cPieces);
		void ParseLanes(const jsoncons::json& cLanes);
		void ParseCars(const jsoncons::json& cCars);
		double CalculateCarSpeed(const jsoncons::json& cCar);
		void GetNextBendInfo(int nTrackPieceIndex, int nLaneIndex, int* pnBendIndex, double* pdDistance) const;

		MessageVector OnJoin(const jsoncons::json& cMessage);
		MessageVector OnYourCar(const jsoncons::json& cMessage);
		MessageVector OnGameInit(const jsoncons::json& cMessage);
		MessageVector OnGameStart(const jsoncons::json& cMessage);
		MessageVector OnCarPositions(const jsoncons::json& cMessage);
		MessageVector OnCrash(const jsoncons::json& cMessage);
		MessageVector OnGameEnd(const jsoncons::json& cMessage);
		MessageVector OnError(const jsoncons::json& cMessage);

	private:
		const std::map<std::string, ActionFunction> m_cActionMap;

		std::string m_strName;
		std::string m_strColor;
		SCarInfo m_sCarInfo;

		std::vector<STrackPiece> m_cTrackPieces;
		std::vector<SLane> m_cLanes;

		int m_nLastTrackPieceIndex;
		double m_dLastInPieceDistance;
		double m_dSpeed;

	}; // class CGameLogic

} // namespace HWO

#endif // HWO_GAME_LOGIC_H

