#include "Protocol.h"

jsoncons::json HWO::Protocol::MakeRequest(const std::string& strMessageType, const jsoncons::json& cData)
{
	jsoncons::json cRequest;
	cRequest["msgType"] = strMessageType;
	cRequest["data"] = cData;
	return cRequest;
}

jsoncons::json HWO::Protocol::MakeJoin(const std::string& strName, const std::string& strKey)
{
	jsoncons::json cBotId;
	cBotId["name"] = strName;
	cBotId["key"] = strKey;
/*
	jsoncons::json cData;
	cData["botId"] = cBotId;
	cData["trackName"] = "keimola";
	cData["trackName"] = "germany";
	cData["trackName"] = "usa";
	cData["trackName"] = "france";
	//cData["carCount"] = "2";
	return MakeRequest("joinRace", cData);
*/
	return MakeRequest("join", cBotId);
}

jsoncons::json HWO::Protocol::MakePing()
{
	return MakeRequest("ping", jsoncons::null_type());
}

jsoncons::json HWO::Protocol::MakeThrottle(double dThrottle)
{
	return MakeRequest("throttle", dThrottle);
}

jsoncons::json HWO::Protocol::MakeSwitchLane(const std::string& strDirection)
{
	return MakeRequest("switchLane", strDirection);
}

