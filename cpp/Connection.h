#ifndef HWO_CONNECTION_H
#define HWO_CONNECTION_H

#include <string>
#include <iostream>
#include <boost/asio.hpp>
#include <jsoncons/json.hpp>

namespace HWO
{
	class CConnection
	{
	public:
		CConnection(const std::string& strHost, const std::string& strPort);
		~CConnection();

		jsoncons::json ReceiveResponse(boost::system::error_code& rcError);
		void SendRequests(const std::vector<jsoncons::json>& cMessages);

	private:
		boost::asio::io_service m_cIoService;
		boost::asio::ip::tcp::socket m_cSocket;
		boost::asio::streambuf m_cResponseBuffer;

	}; // class CConnection

} // namespace HWO

#endif // define(HWO_CONNECTION_H)

