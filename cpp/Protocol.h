#ifndef HWO_PROTOCOL_H
#define HWO_PROTOCOL_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>

namespace HWO
{
	namespace Protocol
	{
		jsoncons::json MakeRequest(const std::string& strMessageType, const jsoncons::json& cData);
		jsoncons::json MakeJoin(const std::string& strName, const std::string& strKey);
		jsoncons::json MakePing();
		jsoncons::json MakeThrottle(double dThrottle); // [0, 1]
		jsoncons::json MakeSwitchLane(const std::string& strDirection); // "Left" or "Right"

	} // namespace Protocol

} // namespace HWO

#endif // define(HWO_PROTOCOL_H)

